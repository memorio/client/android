package com.vegst.memorio.net;

import android.os.AsyncTask;

import com.vegst.memorio.model.User;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FetchUserTask extends AsyncTask<Void, Void, User> {


    @Override
    protected User doInBackground(Void... params) {
        String urlString = "http://192.168.11.3:3000/user";

        try {

            URL url = new URL(urlString);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("GET");

            urlConnection.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));

            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }

            reader.close();


            // JSON parsing
            JSONObject object = new JSONObject(sb.toString());


            return new User(object.getString("username"));

        } catch (Exception e) {
            return null;

        }
    }

}