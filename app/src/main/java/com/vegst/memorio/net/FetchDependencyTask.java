package com.vegst.memorio.net;

import android.os.AsyncTask;
import android.os.SystemClock;

import com.vegst.memorio.model.Dependency;
import com.vegst.memorio.model.Representation;
import com.vegst.memorio.model.Topic;
import com.vegst.memorio.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FetchDependencyTask extends AsyncTask<Void, Void, Dependency> {


    @Override
    protected Dependency doInBackground(Void... params) {
        String urlString = "http://192.168.11.3:3000/dependency";

        try {
            SystemClock.sleep(1000);

            URL url = new URL(urlString);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("GET");

            urlConnection.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));

            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }

            reader.close();


            // JSON parsing
            JSONObject object = new JSONObject(sb.toString());

            return parseDependency(object);

        } catch (Exception e) {
            return null;

        }
    }

    public static Dependency parseDependency(JSONObject object) throws JSONException {
        return new Dependency(object.getString("id"),
                parseTopic(object.getJSONObject("parent_topic")),
                parseTopic(object.getJSONObject("child_topic")));
    }

    public static Topic parseTopic(JSONObject object) throws JSONException {
        return new Topic(object.getString("id"),
                parseRepresentation(object.getJSONObject("representation")));
    }

    public static Representation parseRepresentation(JSONObject object) throws JSONException {
        if (object.getString("type").equals("text")) {
            return new Representation.Text(object.getString("id"), object.getString("data"));
        }
        return null;
    }

}