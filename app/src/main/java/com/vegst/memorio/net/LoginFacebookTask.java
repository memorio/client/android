package com.vegst.memorio.net;


import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginFacebookTask extends AsyncTask<Void, Void, Boolean> {

    private final String mAccessToken;

    public LoginFacebookTask(String accessToken) {
        mAccessToken = accessToken;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        String urlString = "http://192.168.11.3:3000/login/facebook?access_token="+mAccessToken;

        System.out.println(mAccessToken);
        try {

            URL url = new URL(urlString);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            BufferedWriter writer = new BufferedWriter (new OutputStreamWriter(new BufferedOutputStream(urlConnection.getOutputStream()), "UTF-8"));

            writer.flush();

            urlConnection.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }

            writer.close();
            reader.close();


        } catch (Exception e) {

            return false;

        }

        return true;
    }
}
