package com.vegst.memorio.net;

import android.os.AsyncTask;

import com.vegst.memorio.R;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Represents an asynchronous login/registration task used to authenticate
 * the user.
 */
public class LoginLocalTask extends AsyncTask<Void, Void, Boolean> {

    private final String mUsername;
    private final String mPassword;

    public LoginLocalTask(String username, String password) {
        mUsername = username;
        mPassword = password;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        String urlString = "http://192.168.11.3:3000/login/local?username="+mUsername+"&password="+mPassword;
        CookieHandler.setDefault(new CookieManager());
        try {

            URL url = new URL(urlString);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");

            urlConnection.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));

            reader.close();
            return true;

        } catch (Exception e) {
            return false;

        }
    }

}