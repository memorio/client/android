package com.vegst.memorio.model;


public class User {
    private final String mUsername;

    public User(String username) {
        mUsername = username;
    }

    public String getUsername() {
        return mUsername;
    }
}
