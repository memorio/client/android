package com.vegst.memorio.model;


public abstract class Representation {

    private final String mId;

    public Representation(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public static class Text extends Representation {
        private final String mData;

        public Text(String id, String data) {
            super(id);
            mData = data;
        }

        public String getData() {
            return mData;
        }
    }
}
