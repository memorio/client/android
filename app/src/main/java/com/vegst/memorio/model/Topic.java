package com.vegst.memorio.model;


import java.io.Serializable;

public class Topic implements Serializable {

    private final String mId;
    private final Representation mRepresentation;

    public Topic(String id, Representation representation) {
        mId = id;
        mRepresentation = representation;
    }

    public String getId() {
        return mId;
    }

    public Representation getRepresentation() {
        return mRepresentation;
    }
}
