package com.vegst.memorio.model;

import java.io.Serializable;

public class Dependency implements Serializable {

    private final String mId;
    private final Topic mParentTopic;
    private final Topic mChildTopic;

    public Dependency(String id, Topic parentTopic, Topic childTopic) {
        mId = id;
        mParentTopic = parentTopic;
        mChildTopic = childTopic;
    }

    public String getId() {
        return mId;
    }

    public Topic getParentTopic() {
        return mParentTopic;
    }

    public Topic getChildTopic() {
        return mChildTopic;
    }
}
