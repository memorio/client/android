package com.vegst.memorio;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.vegst.memorio.net.LoginFacebookTask;
import com.vegst.memorio.net.LoginLocalTask;

public class LoginActivity extends AppCompatActivity {

    private LoginLocalTask mLoginLocalTask = null;
    private LoginFacebookTask mLoginFacebookTask = null;

    private AutoCompleteTextView mUsernameView;
    private EditText mPasswordView;

    private CallbackManager mCallbackManager;
    private CoordinatorLayout mCoordinatorLayout;
    private LoginButton mFacebookLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        // Set up the login form.
        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });


        mCallbackManager = CallbackManager.Factory.create();

        mFacebookLoginButton = (LoginButton) findViewById(R.id.button_facebook_login);

        mFacebookLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mLoginFacebookTask = new LoginFacebookTask(loginResult.getAccessToken().getToken().toString()) {
                    @Override
                    protected void onPostExecute(Boolean success) {
                        if (success) {
                            Snackbar.make(mCoordinatorLayout, "success", Snackbar.LENGTH_LONG).show();

                        }
                    }
                };
                mLoginFacebookTask.execute((Void) null);
            }

            @Override
            public void onCancel() {
                Snackbar.make(mCoordinatorLayout, "cancel", Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Snackbar.make(mCoordinatorLayout, "error", Snackbar.LENGTH_LONG).show();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private void attemptLogin() {

        String email = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        mLoginLocalTask = new LoginLocalTask(email, password) {
            @Override
            protected void onPostExecute(Boolean success) {
                if (success) {
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                }
                else {
                    Snackbar.make(mCoordinatorLayout, "Failed to log in", Snackbar.LENGTH_LONG).show();
                }
            }
        };
        mLoginLocalTask.execute((Void) null);
    }



}

