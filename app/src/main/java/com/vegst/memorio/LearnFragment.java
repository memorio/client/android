package com.vegst.memorio;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.vegst.memorio.model.Dependency;
import com.vegst.memorio.model.Representation;
import com.vegst.memorio.model.Topic;
import com.vegst.memorio.net.FetchDependencyTask;

public class LearnFragment extends AnimatableFragment {

    private DependencyFragment mFragment;
    private FloatingActionButton mRevealButton;
    private SeekBar mRatingSeekbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LinearLayout layout = (LinearLayout) super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_learn, container, false);

        layout.addView(view);

        mRevealButton = (FloatingActionButton) view.findViewById(R.id.reveal);
        mRevealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFragment != null) {
                    mFragment.reveal();
                    hide(mRevealButton);
                    show(mRatingSeekbar);

                    // Animate
                    ObjectAnimator imageViewObjectAnimator = ObjectAnimator.ofFloat(mRevealButton , "rotation", 0f, 10.0f);
                    imageViewObjectAnimator.setDuration(100);
                    imageViewObjectAnimator.start();
                }
            }
        });

        mRatingSeekbar = (SeekBar) view.findViewById(R.id.rating);
        mRatingSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                hide(mRatingSeekbar);
                mFragment.rate(seekBar.getProgress() / 100.0f);
                (new FetchDependencyTask() {
                    @Override
                    protected void onPostExecute(Dependency dependency) {
                        if (dependency != null) {
                            mFragment = setDependency(dependency); // new Dependency("sd", new Topic("sd", new Representation.Text("s", "front")), new Topic("sd", new Representation.Text("s", "back")))
                            show(mRevealButton);
                        }
                    }
                }).execute((Void) null);
            }
        });

        hide(mRatingSeekbar);
        hide(mRevealButton);

        // TEMP
        mFragment = setDependency(new Dependency("sd", new Topic("sd", new Representation.Text("s", "front")), new Topic("sd", new Representation.Text("s", "back"))));
        show(mRevealButton);

        return layout;
    }

    private DependencyFragment setDependency(Dependency dependency) {
        DependencyFragment fragment = new DependencyFragment();

        Bundle args = new Bundle();
        args.putSerializable("dependency", dependency);
        fragment.setArguments(args);

        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.animator.card_zoom_in,
                        R.animator.card_slide_out_right)
                .replace(R.id.content, fragment)
                .addToBackStack(null)
                .commit();
        return fragment;
    }

    private void show(final View view) {
        view.setVisibility(View.VISIBLE);
        view.animate()
            .translationY(0)
            .setInterpolator(new OvershootInterpolator())
            .setStartDelay(getResources().getInteger(R.integer.card_flip_time_half))
            .setDuration(getResources().getInteger(R.integer.card_flip_time_half))
            .alpha(1.0f)
            .setListener(null);
    }

    private void hide(final View view) {
        view.animate()
            .translationY(view.getHeight())
            .setStartDelay(0)
            .setDuration(getResources().getInteger(R.integer.card_flip_time_half))
            .alpha(0.0f)
            .setInterpolator(new AnticipateInterpolator())
            .setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
    }

    public static class DependencyFragment extends AnimatableFragment {

        Dependency mDependency;
        TopicFragment mFragment;

        public void reveal() {
            mFragment = new TopicFragment();

            Bundle args = new Bundle();
            args.putSerializable("topic", mDependency.getChildTopic());
            mFragment.setArguments(args);

            getChildFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(
                            R.animator.card_flip_left_in,
                            R.animator.card_flip_right_out)
                    .replace(R.id.content, mFragment)
                    .addToBackStack(null)
                    .commit();
        }

        public int blendColorHsl(int color1, int color2, float distribution) {
            float[] color1_hsl = new float[3];
            float[] color2_hsl = new float[3];
            float[] target_color_hsl = new float[3];

            ColorUtils.colorToHSL(color1, color1_hsl);
            ColorUtils.colorToHSL(color2, color2_hsl);

            ColorUtils.blendHSL(color1_hsl, color2_hsl, distribution, target_color_hsl);

            return ColorUtils.HSLToColor(target_color_hsl);
        }


        public void rate(final float rating) {
            final int target_color = blendColorHsl(Color.RED, Color.GREEN, rating);

            ValueAnimator animation = ValueAnimator.ofFloat(0.0f, 1.0f);
            animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator updatedAnimation) {
                    float animatedValue = (float)updatedAnimation.getAnimatedValue();

                    int color = ColorUtils.blendARGB(Color.WHITE, target_color, animatedValue);
                    ImageView background = (ImageView)mFragment.getView().findViewById(R.id.background);

                    background.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
                    background.setAlpha(animatedValue / 2.0f);
                }
            });
            animation.setDuration(300);
            animation.start();

            /*
            ObjectAnimator backgroundColorAnimator = ObjectAnimator.ofObject(,
                    "colorFilter",
                    new ArgbEvaluator(),
                    Color.WHITE,
                    ,
                    PorterDuff.Mode.MULTIPLY);
            backgroundColorAnimator.setDuration(300);
            backgroundColorAnimator.start();
            */
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            LinearLayout layout = (LinearLayout) super.onCreateView(inflater, container, savedInstanceState);

            View view = inflater.inflate(R.layout.fragment_dependency, container, false);

            layout.addView(view);

            mDependency = (Dependency)getArguments().getSerializable("dependency");

            mFragment = new TopicFragment();

            Bundle args = new Bundle();
            args.putSerializable("topic", mDependency.getParentTopic());
            mFragment.setArguments(args);

            getChildFragmentManager().beginTransaction()
                    .add(R.id.content, mFragment)
                    .commit();

            return layout;
        }
    }

    public static class TopicFragment extends AnimatableFragment {

        private TextView mRepresentationText;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            LinearLayout layout = (LinearLayout) super.onCreateView(inflater, container, savedInstanceState);

            View view = inflater.inflate(R.layout.fragment_topic, container, false);

            layout.addView(view);

            Topic topic = (Topic)getArguments().getSerializable("topic");

            mRepresentationText = (TextView) view.findViewById(R.id.representation_text);

            if (topic.getRepresentation() instanceof Representation.Text) {
                mRepresentationText.setText(((Representation.Text)topic.getRepresentation()).getData());
            }

            return layout;
        }
    }
}