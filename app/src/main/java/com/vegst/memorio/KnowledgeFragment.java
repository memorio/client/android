package com.vegst.memorio;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.vegst.memorio.model.Dependency;
import com.vegst.memorio.model.Representation;
import com.vegst.memorio.model.Topic;
import com.vegst.memorio.net.FetchDependencyTask;

public class KnowledgeFragment extends AnimatableFragment {

    private RecyclerView mList;
    private RecyclerView.Adapter mListAdapter;
    private RecyclerView.LayoutManager mListLayoutManager;

    // TEMP
    private String[] data = {"sdf", "sdjfk", "test"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LinearLayout layout = (LinearLayout) super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_knowledge, container, false);

        layout.addView(view);

        mList = (RecyclerView) view.findViewById(R.id.list);
        mList.setHasFixedSize(true);

        mListLayoutManager = new LinearLayoutManager(getActivity());
        mList.setLayoutManager(mListLayoutManager);

        mListAdapter = new MyAdapter(data);
        mList.setAdapter(mListAdapter);

        return layout;
    }

    public static class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

        private String[] mDataset;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public TextView mTextView;
            public ViewHolder(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.text);
            }
        }

        public MyAdapter(String[] myDataset) {
            mDataset = myDataset;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_knowledge, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.mTextView.setText(mDataset[position]);

        }

        @Override
        public int getItemCount() {
            return mDataset.length;
        }
    }

}