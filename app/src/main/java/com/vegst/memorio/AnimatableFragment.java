package com.vegst.memorio;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class AnimatableFragment extends Fragment {

    private View cachedView;
    private Bitmap b = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout layout = new LinearLayout(container.getContext());
        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setId(android.R.id.list);
        return layout;
    }

    @Override
    public void onResume() {
        if (b != null && !b.isRecycled()) {
            b.recycle();
            b = null;
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        b = loadBitmapFromView(getView());
        super.onPause();
    }

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getWidth(),
                v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getWidth(),
                v.getHeight());
        v.draw(c);
        return b;
    }

    @Override
    public void onDestroyView() {
        BitmapDrawable bd = new BitmapDrawable(b);
        cachedView = getView().findViewById(android.R.id.list);
        if (cachedView != null) {
            cachedView.setBackgroundDrawable(bd);
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (b != null && !b.isRecycled()) {
            if (getView() != null && cachedView != null) {
                cachedView.setBackgroundDrawable(null);
            }
            b.recycle();
        }
        b = null;
        super.onDestroy();
    }

}